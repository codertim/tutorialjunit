package com.javacodegeeks.junit;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class JUnitFirstTest {

	@Test
	public void test() {
		fail("Not yet implemented");
	}
	
	@Before
	public void executedBeforeEach() {
		System.out.println("In @Before method");
	}
	
	@AfterClass
	public static void thisWillBeExecutedOnceAfterwards() {
		System.out.println("In @AfterClass method");
	}

	@Test
	// @Ignore
	public void myFirstTest() {
		System.out.println("In my first @Test method");
	}
	
	@Test
	public void testEquals() {
		System.out.println("In testEquals");
		assertEquals(2, (new Integer(2)).intValue());
	}
	
	@Test
	public void testTrue() {
		System.out.println("In testTrue");
		assertTrue(5 == 5);
	}

}
